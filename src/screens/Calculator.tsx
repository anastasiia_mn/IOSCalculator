import React, { useMemo, useState } from "react";
import { View, Text, StyleSheet, useWindowDimensions } from "react-native";
import CalculatorButton from "../components/CalculatorButton";
import {Decimal} from 'decimal.js';
import { customTheme } from "../components/CustomTheme";

interface CalculatorState {
  currentInput: string;
  previousInput: string;
  operation: string | null;
}

const Calculator: React.FC = () => {
  const theme = customTheme();
  const styles = useMemo(() => createStyles(theme), [theme])
  const width = useWindowDimensions().width;
  const inputPadding = width * 0.2 / 2.5;

  const [state, setState] = useState<CalculatorState>({
    currentInput: '',
    previousInput: '',
    operation: null,
  });

  const calculateResult = () => {
    // Для точности данных используем методы из библиотеки Decimal.js
    let result;
    const current = new Decimal(state.currentInput);
    const previous = new Decimal(state.previousInput);

    switch (state.operation) {
      case '+':
        result = previous.plus(current);
        break;
      case '-':
        result = previous.minus(current);
        break;
      case '×':
        result = previous.times(current);
        break;
      case '÷':
        if (current.equals(0)) {
          return;
        }
        result = previous.dividedBy(current);
        break;
      default:
        return;
    }
    return result.toString();
  };

  /** @handlePress
    * string label - значение нажатия
    * Switch-Case Функиця для определения математической операции
   */
  const handlePress = (label: string) => {
    switch (label) {
      case 'AC':
        setState({ currentInput: '', previousInput: '', operation: null });
        break;
      case '+/-':
        // Менять знак значения
        if (state.currentInput) {
          const newValue = (parseFloat(state.currentInput) * -1).toString();
          setState(prevState => ({
            ...prevState,
            currentInput: newValue,
          }));
        }
        break;
      case '%':
        if (state.currentInput) {
          const newValue = (parseFloat(state.currentInput) / 100).toString();
          setState(prevState => ({
            ...prevState,
            currentInput: newValue,
          }));
        }
        break;
      case '+':
      case '-':
      case '×':
      case '÷':
        if (state.currentInput) {
          setState({
            currentInput: state.previousInput,
            previousInput: state.currentInput,
            operation: label,
          });
        } else if (state.previousInput && !state.operation) {
          setState(prevState => ({
            ...prevState,
            operation: label,
          }));
        }
        break;
      case '=':
        if (state.operation && state.currentInput && state.previousInput) {
          const result = calculateResult();
          if (result !== undefined) {
            setState({
              currentInput: result,
              previousInput: '',
              operation: null,
            });
          }
        }
        break;
      default:
        if (label === '.' && state.currentInput.includes('.')) {
          return;
        }
        setState(prevState => ({
          ...prevState,
          currentInput: prevState.currentInput + label,
        }));
        break;
    }
  };

  const buttons = [
    ['AC', '+/-', '%', '÷'],
    ['7', '8', '9', '×'],
    ['4', '5', '6', '-'],
    ['1', '2', '3', '+'],
    ['0', '.', '='],
  ];

  return (
    <View style={styles.container}>
      <View style={[styles.displayContainer, {paddingRight: inputPadding}]}>
        <Text style={styles.displayText}>{state.currentInput}</Text>
      </View>
      <View style={styles.buttonsContainer}>
        {buttons.map((row, rowIndex) => (
          <View key={rowIndex} style={styles.buttonRow}>
            {row.map((buttonLabel) => (
              <CalculatorButton
                key={buttonLabel}
                label={buttonLabel}
                onPress={handlePress}
              />
            ))}
          </View>
        ))}
      </View>
    </View>
  );
};

const createStyles = (theme:any) => StyleSheet.create({
  container: {
    flex: 1,
  },
  displayContainer: {
    flex: 3,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    backgroundColor: theme.colors.backgroundColor,
  },
  displayText: {
    color: theme.colors.textColor,
    fontSize: 65,
  },
  buttonsContainer: {
    flex: 5,
    backgroundColor: theme.colors.backgroundColor,
  },
  buttonRow: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
});

export default Calculator;
