import React, { useMemo } from "react";
import { TouchableOpacity, Text, StyleSheet, useWindowDimensions } from "react-native";
import { customTheme } from "./CustomTheme";

interface CalculatorButtonProps {
  label: string;
  onPress: (label: string) => void;
}

const CalculatorButton: React.FC<CalculatorButtonProps> = ({label, onPress}) => {
  const theme = customTheme();
  const width = useWindowDimensions().width;
  const buttonWidth = width * 0.2;
  const buttonHeight =  width * 0.2;
  const zeroButtonWidth =  width / 2.2;
  const styles = useMemo(() => createStyles(theme, label, buttonWidth, buttonHeight, zeroButtonWidth), [theme, label]);

  const getButtonBackgroundColor = (theme: any, label:string) => {
    if (label === 'AC' || label === '+/-' || label === '%') {
      return theme.colors.buttonColor.tertiary;
    }
    if (label === '+' || label === '-' || label === '×' || label === '÷' || label === '=') {
      return theme.colors.buttonColor.secondary;
    }
    return theme.colors.buttonColor.primary;
  };

  return (
    <TouchableOpacity
      style={[styles.button, {backgroundColor: getButtonBackgroundColor(theme, label)}]}
      onPress={() => onPress(label)}>
      <Text style={styles.text}>{label}</Text>
    </TouchableOpacity>
  );
};

const createStyles = (theme: any, label:string, buttonWidth:number, buttonHeight: number, zeroButtonWidth: number) => StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems:  label === '0' ? ('flex-start') : 'center',
    paddingLeft: label === '0' ? (buttonWidth / 2.5) : 0,
    borderRadius: 90,
    width: label === '0' ? zeroButtonWidth : buttonWidth,
    height: buttonHeight,
  },
  text: {
    fontSize: 24,
    fontWeight: 'bold',
    color: theme.colors.textColor,
  },
});

export default CalculatorButton;
