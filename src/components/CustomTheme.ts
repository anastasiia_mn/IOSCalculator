import { DefaultTheme } from '@react-navigation/native';

export const customTheme = () => {
  return {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      backgroundColor: '#000',
      textColor: '#fff',
      buttonColor: {
        primary: '#484747',
        secondary: '#FF9500',
        tertiary: '#a2a0a0'
      },
    }
  }
}
