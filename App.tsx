/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import {SafeAreaView} from 'react-native';
import Calculator from './src/screens/Calculator';

const App: React.FC = () => {
  return (
    <SafeAreaView style={{flex: 1}}>
      <Calculator />
    </SafeAreaView>
  );
};
export default App;
